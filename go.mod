module gitlab.com/akvo/subpub

go 1.14

require (
	github.com/ethereum/go-ethereum v1.9.11
	github.com/ipfs/go-cid v0.0.5
	github.com/libp2p/go-libp2p v0.6.0
	github.com/libp2p/go-libp2p-core v0.5.0
	github.com/libp2p/go-libp2p-crypto v0.1.0
	github.com/libp2p/go-libp2p-discovery v0.2.0
	github.com/libp2p/go-libp2p-kad-dht v0.5.1
	github.com/multiformats/go-multiaddr v0.2.1
	gitlab.com/vocdoni/go-dvote v0.0.0-20200307090701-c6ddddca41f5
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
