package main

import (
	"encoding/json"
	"strconv"
	"time"

	"gitlab.com/akvo/subpub/p2p"

	flag "github.com/spf13/pflag"

	"gitlab.com/vocdoni/go-dvote/crypto/signature"
	"gitlab.com/vocdoni/go-dvote/log"
	"gitlab.com/vocdoni/go-dvote/util"
)

type Message struct {
	Type      string `json:"type"`
	Timestamp string `json:"timestamp"`
	PublicKey string `json:"publickey"`
	Data      string `json:"data"`
}

func (t *Message) Decode(b []byte) error {
	return json.Unmarshal(b, t)
}

func (t *Message) Bytes() []byte {
	b, err := json.Marshal(t)
	if err != nil {
		log.Error(err)
	}
	return b
}

func broadcastUpdater(period time.Duration, cp p2p.SubPub) {
	for {
		ip, _ := util.PublicIP()
		var m Message

		m.Type = "update"
		m.Timestamp = strconv.FormatInt(time.Now().Unix(), 10)
		m.PublicKey = cp.PubKey
		m.Data = ip.String()

		cp.BroadcastWriter <- m.Bytes()
		time.Sleep(period)
	}
}

func main() {
	key := flag.String("key", "", "private key (leave blank for auto-generate")
	groupKey := flag.String("groupKey", "", "p2p group key")
	port := flag.Int32("port", 45678, "network port to listen on")

	flag.Parse()
	log.InitLogger("debug", "stdout")
	var keys signature.SignKeys
	if len(*key) == 0 {
		if err := keys.Generate(); err != nil {
			log.Fatal(err)
		}
	} else {
		if err := keys.AddHexKey(*key); err != nil {
			log.Fatal(err)
		}
	}
	cp := p2p.NewSubPub(keys.Private, *groupKey, *port)

	// Start the broadcast updater
	go broadcastUpdater(time.Second*10, cp)

	// Start the SubPub
	cp.Connect()
	cp.Subcribe()

}
